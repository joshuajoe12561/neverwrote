const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.send('This is an API. Please hit one of my endpoints, like `/notebooks`.');
});
router.delete('/', (req, res) => {
  res.send('will be deleted `/notebooks`');
});
router.put('/', (req, res) => {
  res.send('`/notebook`');
});
router.post('/', (req, res) => {
  res.send("will be created..");
});
module.exports = router;
