const bodyParser = require('body-parser');
const express = require('express');
const _ = require('lodash');
const models = require('../models');

const router = express.Router();

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());
router.use(bodyParser.raw());

// Index
router.get('/', (req, res) => {
  models.Notebook.findAll({ order: [['createdAt', 'DESC']] })
    .then(notebooks => res.json(notebooks))
    .catch(err => res.status(500).json({ error: err.message }));
});
router.post('/', (req, res) => {
  models.Notebook.create({order: [['createdAt', 'DESC']] })
    .then(notebooks => res.json(notebooks))
    .catch(err => res.status(500).json({ error: err.message }));
});
router.put('/', (req, res) => {
  models.Notebook.update({ order: [['createdAt', 'DESC']] })
    .then(notebooks => res.json(notebooks))
    .catch(err => res.status(500).json({ error: err.message }));
});
router.delete('/', (req, res) => {
  models.Notebook.destroy({ order: [['createdAt', 'DESC']] })
    .then(
      res.sendStatus(200))
    .catch(err => res.status(500).json({ error: err.message }));
});
//get notebook by id
router.get('/notebooks/:notebookId', (req, res) => {
  var reqData = req.body;
   models.Notebook.findAll({where: {id: reqData.notebookId}})
   .then(notebooks => res.json(notebooks))
   .catch(err => res.status(500).json({error: err.message}));
});
//creates a new notebook
router.post('/notebooks', (req, res) => {
  var reqData = req.body;
  models.Notebook.create({title : reqData.Title})
  .then(noteBook => {res.json(noteBook)})
  .catch(err => res.status(500).json({ error: err.message }));
});
//updates notebook by the id
router.put('notebooks/:notebookId', (req, res) => {
  var reqData = req.body;
  models.Notebook.findByPk(reqData.notebookId)
  .then(
    function(notebook){
      notebook.update({title: reqData.Title
      });
    }
  ).then(noteBooks => {res.json(noteBooks)
  }).catch(err => res.status(500).json({error: err.message }));
});
//deletes notebook by the id 
router.delete('/notebooks/:notebookId', (req, res) => {
var reqData = req.body;
  models.Notebook.findByPk(reqData.notebookId)
  .then(
    function(notebook){
      notebook.destroy();
    }
  ).then(
    res.sendStatus(200)
  ).catch(err => res.status(500).json({error: err.message }));
});
/* *** TODO: Fill in the API endpoints for notebooks *** */

module.exports = router;
