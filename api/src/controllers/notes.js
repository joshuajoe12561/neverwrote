const bodyParser = require('body-parser');
const express = require('express');
const _ = require('lodash');
const models = require('../models');

const router = express.Router();
router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());
router.use(bodyParser.raw());

// Index
router.get('/', (req, res) => {
  models.Note.findAll({ order: [['createdAt', 'DESC']] })
    .then(notes => res.json(notes))
    .catch(err => res.status(500).json({ error: err.message }));
});
router.post('/', (req, res) => {
  models.Note.create({order: [['createdAt', 'DESC']] })
    .then(notes => res.json(notes))
    .catch(err => res.status(500).json({ error: err.message }));
});
router.put('/', (req, res) => {
  models.Note.update({order: [['createdAt', 'DESC']] })
    .then(notes => res.json(notes))
    .catch(err => res.status(500).json({ error: err.message }));
});
router.delete('/', (req, res) => {
  models.Note.destroy({ order: [['createdAt', 'DESC']] })
    .then(notes => res.json(notes))
    .catch(err => res.status(500).json({ error: err.message }));
});
//get note by id
router.get('/notes/:noteId', (req, res) => {
  models.Note.findAll({where: {id: req.params.noteId}})
  .then(notes => res.json(notes))
  .catch(err => res.status(500).json({error: err.message}));
});
//get notes by notebookId 
router.get('/notebooks/:notebookId/notes', (req, res) => {
  models.Note.findAll({where: {notebookId: req.params.notebookId}})
  .then(notes => res.json(notes))
  .catch(err => res.status(500).json({error: err.message}));
});
//creates a new note
router.post('/notes', (req, res) => {
  var reqData = req.body;
  models.Note.create({
      title : reqData.title,
      content: reqData.content,
      notebookId: reqData.notebookId
    })
  .then(notes => { res.json(notes)})
  .catch(err => res.status(500).json({ error: err.message }));
});
//updates note by the id
router.put('/notes/:notesId', (req, res) => {
  var reqData = req.body;
  models.Note.findByPk(reqData.notesId).then(
    function(note){
      note.update({
        title: reqData.title,
        content: reqData.content,
        notebookId: reqData.notebookId
      })
      .then(notes => { res.json(notes)})
  .catch(err => res.status(500).json({ error: err.message }));
    }); 	
});
//deletes note by id
router.delete('/notes/:noteId', (req, res) => {
  models.Note.findByPk(req.params.noteId).then(function(note){
    note.destroy();
  })
  .then(notes => { res.json(notes)})
  .catch(err => res.status(500).json({ error: err.message })); 	
});
/* *** TODO: Fill in the API endpoints for notes *** */

module.exports = router;
