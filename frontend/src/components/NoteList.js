const React = require('react');
const ReactRedux = require('react-redux');

const createActionDispatchers = require('../helpers/createActionDispatchers');
const notesActionCreators = require('../reducers/notes');

/*
  *** TODO: Build more functionality into the NotebookList component ***
  At the moment, the NotebookList component simply renders the notebooks
  as a plain list containing their titles. This code is just a starting point,
  you will need to build upon it in order to complete the assignment.
*/
class NoteList extends React.Component {
  render() {
    const createNoteContent = (note) => {
        return( 
            <p key={note.title}>
                {note.content}
            </p>
        )
    }
    const createNoteListItem = (note) => {
      return (
        <li key={note.id}>
          {note.title}
        </li>
      )
    }


    return (
      <div>
        <h2>Notes</h2>
        <ul>
          {this.props.notes.data.map(createNoteListItem)}
        </ul>
      </div>
    );
  }
}

const NoteListContainer = ReactRedux.connect(
  state => ({
    notes: state.notes
  }),
  createActionDispatchers(notesActionCreators)
)(NoteList);

module.exports = NoteListContainer;
