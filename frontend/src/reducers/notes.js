const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants
const FETCHALLNOTE = 'FETCHALLNOTE';
const CREATENOTE = 'CREATENOTE';
const DELETENOTE = 'DELETENOTE'; 
/* *** TODO: Put action constants here *** */

const initialState = {
  data: [
    { id: 100, title: 'From Redux Store one',content:'A hard-coded notebook' },
    { id: 101, title: 'From Redux Store two',content:'Another hard-coded notebook' },
  ]
};
// Function which takes the current data state and an action,
// and returns a new state
function reducer(state,action){
    state = state || initialState;
    action = action || {};
    
    switch (action.type) {
        case FETCHALLNOTE: [
            ...state,
            {
              title: action.payload.title
            }
          ];
        case CREATENOTE: [
            ...state,
          {
            id: ++lastId,
            title: action.payload.title
          }
          ];
        case DELETENOTE: 
        return state.filter(data => data.id !=action.payload.id);
        
        default: return state;
    }
}

// Action creators
/* *** TODO: Put action creators here *** */
const fetchAllNote = () => ({
    type: FETCHALLNOTE
  });
  const createNote = note =>({
    type: CREATENOTE,
    payload: {note}
  });
  const deleteNote = id =>({
    type: DELETENOTE,
    id
  })
 // Export the action creators and reducer
 module.exports = reducer;
 