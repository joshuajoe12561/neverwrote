const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants
const FETCHALLNOTEBOOK = 'FETCHALLNOTEBOOK';
const CREATENOTEBOOK = 'CREATENOTEBOOK';
const DELETENOTEBOOK = 'DELETENOTEBOOK'; 
/* *** TODO: Put action constants here *** */

const initialState = {
  data: [
    { id: 100, title: 'From Redux Store: A hard-coded notebook' },
    { id: 101, title: 'From Redux Store: Another hard-coded notebook' },
  ]
};

// Function which takes the current data state and an action,
// and returns a new state
function reducer(state, action) {
  state = state || initialState;
  action = action || {};

  switch(action.type) {
    /* *** TODO: Put per-action code here *** */
    case FETCHALLNOTEBOOK : 
    return [
      ...state,
      {
        title: action.payload.title
      }
    ]
    case CREATENOTEBOOK : 
    return [
      ...state,
    {
      id: ++lastId,
      title: action.payload.title
    }
    ];
    case DELETENOTEBOOK : 
    return state.filter(data => data.id !=action.payload.id);
    default: return state;
  }
}

// Action creators
/* *** TODO: Put action creators here *** */
 const fetchAllNotebook = () => ({
   type: FETCHALLNOTEBOOK
 });
 const createNotebook = title =>({
   type: CREATENOTEBOOK,
   title
 });
 const deleteNotebook = id =>({
   type: DELETENOTEBOOK,
   id
 })
// Export the action creators and reducer
module.exports = reducer;
